#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 13:27:16 2015

@author: traca001
"""
import os
import sys
import json
import fileinput
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

debug = ''

def parse_bnx(runsizes = {}):
    for line in fileinput.input():
        if line.startswith('# Run '):
            if debug : print line.strip()
            line = line.split('\t')
            # The SourceFolder of this Run is the 2nd entry of the line
            SourceFolder = line[1]
            if debug : print SourceFolder
            # The RunId of this Run is the last entry of the line
            RunId = int(line[-1])
            if debug : print RunId
            runsizes[RunId]=dict()
            # The NumberofScans of this Run is the 8th entry of the line
            NumberofScans = int(line[7])
            for Scan in range(1, NumberofScans + 1):
                runsizes[RunId][Scan] = 0
            if debug : print runsizes
        elif line.startswith('0\t'):
            line = line.split('\t')
            if debug : print line
            # Length is the 3rd entry
            Length = float(line[2])
            # ScanNumber is the 8th entry
            ScanNumber = int(line[7])
            # RunId is the second to last entry
            RunId = int(line[-2])
            if debug: print ScanNumber, Length
            runsizes[RunId][ScanNumber] += Length
            if debug : print runsizes
    return runsizes

if __name__ == '__main__':
    runsizes = dict()
    # Grab the info from the BNX file(s)
    runsizes = parse_bnx()
    # Dump dictionary into pandas, so we can more easily plot it
    df = pd.DataFrame(runsizes)
    # Plot the dataframe
    df.plot(marker='.', linestyle='-')
    # Set title
    plt.suptitle('Scan number vs. Length', fontsize=14, fontweight='bold')
    # Set labels
    plt.xlabel('Scan number')
    plt.ylabel('Length (bases)')
    # Remove background grid
    plt.grid(b=False)
    # Prettier legend
    plt.legend(loc="upper right",shadow=True, fancybox=True)
    # Store plot
    plt.savefig("scans.png")
